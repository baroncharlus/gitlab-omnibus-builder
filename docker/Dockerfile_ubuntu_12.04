FROM ubuntu:precise
MAINTAINER GitLab Inc. <support@gitlab.com>

# Install required packages
RUN apt-get update -q \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      build-essential \
      zlib1g-dev \
      byacc \
      cmake \
      python-pip \
      git \
      gcc \
      libssl-dev \
      libyaml-dev \
      libffi-dev \
      libreadline-dev \
      libgdbm-dev \
      libncurses5-dev \
      make \
      bzip2 \
      curl \
      ca-certificates \
      locales \
      openssh-server \
      libcurl4-openssl-dev \
      libexpat1-dev \
      gettext \
      libz-dev \
      libssl-dev \
      fakeroot \
      python-dev \
      ccache \
      distcc \
      unzip \
      apt-transport-https

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN pip install -U pip
RUN pip install -U setuptools
RUN pip install awscli

ENV GIT_VERSION 2.7.4
RUN curl -fsSL "https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/git-2.7.4 \
  && ./configure \
  && make all \
  && make install

ENV GO_VERSION 1.8.1
RUN curl -fsSL "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz" \
	| tar -xzC /usr/local \
  && ln -sf /usr/local/go/bin/go /usr/local/go/bin/gofmt /usr/local/go/bin/godoc /usr/local/bin/

ENV RUBY_VERSION 2.3.3
RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/2.3/ruby-${RUBY_VERSION}.tar.gz" \
  | tar -xzC /tmp \
  && cd /tmp/ruby-2.3.3 \
  && ./configure --disable-install-rdoc --disable-install-doc --disable-install-capi\
  && make \
  && make install

ENV RUBYGEMS_VERSION 2.6.8
RUN /usr/local/bin/gem update --system ${RUBYGEMS_VERSION} --no-ri --no-rdoc

ENV BUNDLER_VERSION 1.13.6
RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-ri --no-rdoc

ENV NODE_VERSION 6.9.5
RUN curl -fsSL "https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz" \
  | tar --strip-components 1 -xzC /usr/local/

ENV YARN_VERSION 0.20.3
RUN mkdir /usr/local/yarn \
  && curl -fsSL "https://yarnpkg.com/downloads/${YARN_VERSION}/yarn-v${YARN_VERSION}.tar.gz" \
	| tar -xzC /usr/local/yarn --strip 1 \
  && ln -sf /usr/local/yarn/bin/yarn /usr/local/bin/

RUN mkdir -p /opt/gitlab /var/cache/omnibus ~/.ssh

RUN git config --global user.email "packages@gitlab.com"
RUN git config --global user.name "GitLab Inc."

RUN rm -rf /tmp/*
