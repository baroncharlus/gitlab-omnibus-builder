FROM centos:centos7
MAINTAINER GitLab Inc. <support@gitlab.com>

# Install required packages
RUN yum update -y -q
RUN yum groupinstall -y Development Tools
RUN yum install -y epel-release
RUN yum install -y -q \
      gcc \
      curl-devel \
      expat-devel \
      gettext-devel \
      openssl-devel \
      perl-devel \
      zlib-devel \
      make \
      libyaml-devel \
      libffi-devel \
      readline-devel \
      zlib-devel \
      gdbm-devel \
      ncurses-devel \
      bzip2 \
      which \
      byacc \
      cmake \
      fakeroot \
      python-setuptools  \
      python-setuptools-devel \
      python-devel \
      ccache \
      distcc \
      unzip

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
RUN localedef -c -f UTF-8 -i en_US en_US.UTF-8

RUN easy_install pip
RUN pip install awscli

ENV GIT_VERSION 2.7.4
RUN curl -fsSL "https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/git-2.7.4 \
  && ./configure \
  && make all \
  && make install

ENV GO_VERSION 1.8.1
RUN curl -fsSL "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz" \
	| tar -xzC /usr/local \
  && ln -sf /usr/local/go/bin/go /usr/local/go/bin/gofmt /usr/local/go/bin/godoc /usr/local/bin/

ENV RUBY_VERSION 2.3.3
RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/2.3/ruby-${RUBY_VERSION}.tar.gz" \
  | tar -xzC /tmp \
  && cd /tmp/ruby-2.3.3 \
  && ./configure --disable-install-rdoc --disable-install-doc --disable-install-capi\
  && make \
  && make install

ENV RUBYGEMS_VERSION 2.6.8
RUN /usr/local/bin/gem update --system ${RUBYGEMS_VERSION} --no-ri --no-rdoc

ENV BUNDLER_VERSION 1.13.6
RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-ri --no-rdoc

ENV NODE_VERSION 6.9.5
RUN curl -fsSL "https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz" \
  | tar --strip-components 1 -xzC /usr/local/

ENV YARN_VERSION 0.20.3
RUN mkdir /usr/local/yarn \
  && curl -fsSL "https://yarnpkg.com/downloads/${YARN_VERSION}/yarn-v${YARN_VERSION}.tar.gz" \
	| tar -xzC /usr/local/yarn --strip 1 \
  && ln -sf /usr/local/yarn/bin/yarn /usr/local/bin/

RUN mkdir -p /opt/gitlab /var/cache/omnibus ~/.ssh

RUN git config --global user.email "packages@gitlab.com"
RUN git config --global user.name "GitLab Inc."

RUN rm -rf /tmp/*
